.SILENT:
.PHONY:

include yerlen/.env.local.example

app_dir = yerlen
node_container = local-yerlen
env_file = .env

up:
	docker compose up -d

down:
	docker compose down --remove-orphans

reload: down up

bash:
	docker compose exec $(node_container) bash

install-dependencies:
	docker compose exec -t $(node_container) yarn install

start-nuxt-server:
	docker compose exec -it $(node_container) npx nuxi dev --dotenv $(env_file)

start: up start-nuxt-server
restart: down up start-nuxt-server

init: up install-dependencies start-nuxt-server

.DEFAULT_GOAL := init

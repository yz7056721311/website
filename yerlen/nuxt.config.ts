// https://nuxt.com/docs/api/configuration/nuxt-config
import {defineNuxtConfig} from 'nuxt/config'

export default defineNuxtConfig({
    devtools: {enabled: true},
    ssr:     true,
    modules: [
        "nuxt-icons",
        "@nuxt/content",
        "@nuxtjs/robots",
        "@nuxtjs/i18n",
    ],
    srcDir:  './src',
    appDir:  './src',
    pages:   true,
    app:     {
        head: {
            link: [
                {
                    type: 'image/svg+xml',
                    rel:  'icon',
                    href: '/favicon.svg?version=1'
                },
            ],
            meta: [
                {charset: 'utf-8'},
                {name: 'viewport', content: 'width=device-width, initial-scale=1'},
            ]
        },
    },
    css:     [
        '@/assets/scss/tailwind.scss',
        '@/assets/scss/main.scss',
    ],
    postcss: {
        plugins: {
            'postcss-import':      {},
            'tailwindcss/nesting': {},
            tailwindcss:           {},
            autoprefixer:          {},
        }
    },
    content: {
        watch: false,
    },
    vite:    {
        css: {
            preprocessorOptions: {
                scss: {
                    additionalData: '@use "@/assets/scss/_variables.scss" as *; @use "@/assets/scss/_mixins.scss" as *;',
                },
            },
        },
    },
    robots:  {
        rules: {
            UserAgent: '*',
            Disallow:  '',
        }
    },
    i18n:    {
        locales:       [
            {
                code: 'en',
                file: 'en-US.ts',
            },
            {
                code: 'ru',
                file: 'ru-RU.ts',
            },
        ],
        lazy:          true,
        langDir:       'lang',
        defaultLocale: 'en',
    }
})

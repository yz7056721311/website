export default defineI18nLocale(async locale => {
    return {
        title:       'Ерлен Жубангалиев',
        description: "Веб сайт Ерлена Жубангалиева",
        author:      'Ерлен Жубангалиев',
        foobar:      'заметки разработчика',
        devnet:      'блог разработчиков',
        ru:          'Русский',
        en:          'Английский',
        research: 'Исследую...',
    }
})

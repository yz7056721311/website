export default defineI18nLocale(async locale => {
    return {
        title:       'Yerlen Zhubangaliyev',
        description: "Yerlen Zhubangaliyev's website",
        author:      'Yerlen Zhubangaliyev',
        foobar:      'developer notes',
        devnet:      'developers blog',
        ru:          'Russian',
        en:          'English',
        research: 'Researching...',
    }
})

import type {Config} from 'tailwindcss'

export default <Partial<Config>>{
    content: [
        "./src/components/**/*.{vue,ts}",
        "./src/layouts/**/*.vue",
        "./src/pages/**/*.vue",
        "./src/plugins/**/*.ts",
        "./src/error.vue",
        "./content/**/*.md",
    ],
    theme:   {
        extend: {
            colors: {
                'blue':        '#476470',
                'blue-dark':   '#2E3F46',
                'blue-darker': '#1a2327',
                'blue-light':  '#6993a5',
                'pink':         '#ba70a6',
                'pink-dark':    '#683f5c',
                'pink-light':   '#fa9add',
                'orange':       '#bf6849',
                'orange-dark':  '#6a3b2a',
                'orange-light': '#fa8960',
                'green':        '#68a386',
                'green-dark':   '#3d6150',
                'green-light': '#78a89a',
                'gray-dark':    '#273444',
                'gray':         '#8492a6',
                'gray-light':   '#d3dce6',
            },
        },
    },
    plugins: [],
}
